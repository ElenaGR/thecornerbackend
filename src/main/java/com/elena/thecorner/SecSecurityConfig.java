package com.elena.thecorner;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
public class SecSecurityConfig extends WebSecurityConfigurerAdapter{

	private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
    
    public SecSecurityConfig(UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    
    @Override
    protected void configure(HttpSecurity security) throws Exception{
    	security.httpBasic().and()
    	.authorizeRequests()
    	.antMatchers(HttpMethod.POST, "/admin/**").hasRole("ADMIN")
    	.antMatchers(HttpMethod.GET, "/admin/**").hasRole("ADMIN")
    	.antMatchers(HttpMethod.PUT, "/admin/**").hasRole("ADMIN")
    	.antMatchers(HttpMethod.PATCH, "/admin/**").hasRole("ADMIN")
    	.antMatchers(HttpMethod.DELETE, "/admin/**").hasRole("ADMIN")
    	.antMatchers(HttpMethod.POST, "/admin/**").hasRole("USER")
    	.antMatchers(HttpMethod.GET, "/admin/**").hasRole("USER")
    	.antMatchers(HttpMethod.PUT, "/admin/**").hasRole("USER")
    	.antMatchers(HttpMethod.PATCH, "/admin/**").hasRole("USER")
    	.antMatchers(HttpMethod.DELETE, "/admin/**").hasRole("USER")
    	.and().csrf().disable()
    	.formLogin().disable();
    }
    
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
        .withUser("user").password(this.bCryptPasswordEncoder.encode("password")).roles("USER")
        .and()
        .withUser("admin").password(this.bCryptPasswordEncoder.encode("password")).roles("USER","ADMIN");
    }
}
