package com.elena.thecorner.requests.post;

public class DeleteFileRequest {
	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
