package com.elena.thecorner.requests.post;

public class SeleccionarPostRequest {
	private Long idPost;
	
	public Long getIdPost() {
		return idPost;
	}

	public void setIdPost(Long idPost) {
		this.idPost = idPost;
	}
}
