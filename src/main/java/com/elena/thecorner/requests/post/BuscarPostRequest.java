package com.elena.thecorner.requests.post;

public class BuscarPostRequest {
	private String titulo;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
}
