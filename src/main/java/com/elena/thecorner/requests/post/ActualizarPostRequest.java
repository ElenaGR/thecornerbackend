package com.elena.thecorner.requests.post;

import java.util.List;

public class ActualizarPostRequest {
	private Long idPost;
	
	private Long idCategoria;
	
	private String titulo;
	
	private List<String> parrafos;
	
	private List<String> recursosUrls;

	public Long getIdPost() {
		return idPost;
	}

	public void setIdPost(Long idPost) {
		this.idPost = idPost;
	}

	public Long getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Long idCategoria) {
		this.idCategoria = idCategoria;
	}

	public List<String> getParrafos() {
		return parrafos;
	}

	public void setParrafos(List<String> parrafos) {
		this.parrafos = parrafos;
	}

	public List<String> getRecursosUrls() {
		return recursosUrls;
	}

	public void setRecursosUrls(List<String> recursosUrls) {
		this.recursosUrls = recursosUrls;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	} 
}
