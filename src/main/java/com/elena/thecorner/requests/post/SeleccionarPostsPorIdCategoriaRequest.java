package com.elena.thecorner.requests.post;

public class SeleccionarPostsPorIdCategoriaRequest {
	private Long idCategoria;

	public Long getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Long idCategoria) {
		this.idCategoria = idCategoria;
	}
}
