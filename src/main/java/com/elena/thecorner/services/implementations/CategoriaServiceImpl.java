package com.elena.thecorner.services.implementations;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elena.thecorner.daos.ICategoriaDao;
import com.elena.thecorner.entities.Categoria;
import com.elena.thecorner.services.interfaces.ICategoriaService;

@Service
public class CategoriaServiceImpl implements ICategoriaService{
	
	@Autowired
	ICategoriaDao categoriaDao;

	@Override
	public Categoria save(Categoria categoria) {
		return categoriaDao.save(categoria);
	}

	@Override
	public Optional<Categoria> findById(Long idCategoria) {
		return categoriaDao.findById(idCategoria) ;
	}


}
