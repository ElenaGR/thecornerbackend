package com.elena.thecorner.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elena.thecorner.daos.IComentarioDao;
import com.elena.thecorner.entities.Comentario;
import com.elena.thecorner.services.interfaces.IComentarioService;

@Service
public class ComentarioServiceImpl implements IComentarioService{
	
	@Autowired
	IComentarioDao comentarioDao;

	@Override
	public Comentario save(Comentario comentario) {
		return comentarioDao.save(comentario);
	}
}
