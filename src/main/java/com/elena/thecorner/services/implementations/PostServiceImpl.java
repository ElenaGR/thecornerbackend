package com.elena.thecorner.services.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elena.thecorner.daos.IPostDao;
import com.elena.thecorner.entities.Post;
import com.elena.thecorner.services.interfaces.IPostService;

@Service
public class PostServiceImpl implements IPostService{
	
	@Autowired
	IPostDao postDao;

	@Override
	public Post save(Post post) {
		return postDao.save(post);
	}

	@Override
	public Optional<Post> findById(Long idPost) {
		return postDao.findById(idPost);
	}

	@Override
	public void deleteById(Long idPost) {
		postDao.deleteById(idPost);
	}

	@Override
	public List<Post> seleccionarPostsByIdCategoria(Long idCategoria) {
		return postDao.seleccionarPostsByIdCategoria(idCategoria);
	}

	@Override
	public List<Post> devolverPostsOrdenadosPorFecha() {
		return postDao.devolverPostsOrdenadosPorFecha();
	}

	@Override
	public List<Post> devolverTitulosDePosts(String titulo) {
		return postDao.devolverTitulosDePosts(titulo);
	}
	
}
