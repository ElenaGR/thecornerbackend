package com.elena.thecorner.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elena.thecorner.daos.IUsuarioDao;
import com.elena.thecorner.entities.Usuario;
import com.elena.thecorner.services.interfaces.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService{
	
	@Autowired
	IUsuarioDao usuarioDao;

	@Override
	public Usuario save(Usuario usuario) {
		return usuarioDao.save(usuario);
	}

	@Override
	public Usuario encontarUsuarioPorCorreo(String correo) {
		return usuarioDao.encontarUsuarioPorCorreo(correo);
	}

}
