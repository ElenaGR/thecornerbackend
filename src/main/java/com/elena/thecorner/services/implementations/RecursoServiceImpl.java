package com.elena.thecorner.services.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elena.thecorner.daos.IRecursoDao;
import com.elena.thecorner.entities.Recurso;
import com.elena.thecorner.services.interfaces.IRecursoService;

@Service
public class RecursoServiceImpl implements IRecursoService{
	
	@Autowired
	IRecursoDao recursoDao;

	@Override
	public Recurso save(Recurso recurso) {
		return recursoDao.save(recurso);
	}

	@Override
	public void deleteRecursobyidPost(Long idPost) {
		recursoDao.deleteRecursobyidPost(idPost);
	}

	@Override
	public List<Recurso> seleccionarRecurso(Long idPost) {
		return recursoDao.seleccionarRecurso(idPost);
	}
}
