package com.elena.thecorner.services.interfaces;

import java.util.Optional;

import com.elena.thecorner.entities.Categoria;


public interface ICategoriaService {
	public Categoria save(Categoria categoria);

	public Optional<Categoria> findById(Long idCategoria);

}
