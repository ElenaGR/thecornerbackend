package com.elena.thecorner.services.interfaces;

import java.util.List;

import com.elena.thecorner.entities.Recurso;

public interface IRecursoService {
	public Recurso save(Recurso recurso);
	
	public void deleteRecursobyidPost(Long idPost);
	
	public List<Recurso> seleccionarRecurso(Long idPost);
}
