package com.elena.thecorner.services.interfaces;

import com.elena.thecorner.entities.Comentario;

public interface IComentarioService {
	public Comentario save(Comentario comentario);
}
