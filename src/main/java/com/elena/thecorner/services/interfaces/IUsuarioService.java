package com.elena.thecorner.services.interfaces;

import com.elena.thecorner.entities.Usuario;

public interface IUsuarioService {
	public Usuario save(Usuario usuario);
	
	public Usuario encontarUsuarioPorCorreo(String correo);
}
