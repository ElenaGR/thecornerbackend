package com.elena.thecorner.services.interfaces;

import java.util.List;
import java.util.Optional;

import com.elena.thecorner.entities.Post;

public interface IPostService {
	public Post save(Post post);
	
	Optional<Post> findById(Long idPost);
	
	public void deleteById(Long idPost);
	
	public List<Post> seleccionarPostsByIdCategoria(Long idCategoria);
	
	public List<Post> devolverPostsOrdenadosPorFecha();
	
	public List<Post> devolverTitulosDePosts(String titulo);
}
