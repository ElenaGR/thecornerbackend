package com.elena.thecorner.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.elena.thecorner.entities.Usuario;
import com.elena.thecorner.requests.usuarios.LoginRequest;
import com.elena.thecorner.requests.usuarios.RegisterRequest;
import com.elena.thecorner.responses.MasterResponse;
import com.elena.thecorner.services.interfaces.IUsuarioService;
import com.google.gson.Gson;


@RestController
@RequestMapping(path = "usuario")
public class UsuarioController {
	
	@Autowired
	BCryptPasswordEncoder encoder;
	
	@Autowired
	IUsuarioService usuarioService;
	
	@PostMapping(path = "login", consumes = "application/json", produces = "application/json")
	@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
	public Object login(@RequestBody String json) {
		
		LoginRequest request = new Gson().fromJson(json, LoginRequest.class);
		MasterResponse response = new MasterResponse();
		
		try {
			try {
				Usuario user = usuarioService.encontarUsuarioPorCorreo(request.getEmail());
				
				if(user!=null) {
					if(this.encoder.matches(request.getPassword(), user.getPassword())) {
						response.setMensaje("Login suceessful");
						response.setExitoso(true);
					}else {
						response.setMensaje("Your password is wrong");
						response.setExitoso(false);
					}
				}else {
					response.setMensaje("The email does not exist");
					response.setExitoso(false);
				}
			}catch(Exception e) {
				response.setMensaje("Internal server error at: login method into UserController");
				response.setExitoso(false);
				e.printStackTrace();
			}
		}catch(Exception e) {
			response.setMensaje("Internal server error at: login method into UserController");
			response.setExitoso(false);
			e.printStackTrace();
		}
		
		return response;
	}
	
	@PostMapping(path = "registro", consumes = "application/json", produces = "application/json")
	@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
	public Object registro(@RequestBody String json) {
		
		RegisterRequest request = new Gson().fromJson(json, RegisterRequest.class);
		MasterResponse response = new MasterResponse();
		
		try {
			String passE = encoder.encode(request.getPassword());
			
			Usuario usuario = new Usuario();
			usuario.setCorreo(request.getEmail());
			usuario.setPassword(passE);
			usuarioService.save(usuario);
			
			response.setExitoso(true);
			response.setMensaje("Usuario creado correctamente");
			
		}catch(Exception e) {
			e.printStackTrace();
			response.setExitoso(false);
			response.setMensaje("Error al registrar status 500");
		}
		
		return response;
	}
}
