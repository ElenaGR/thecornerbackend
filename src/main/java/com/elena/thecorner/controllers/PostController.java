package com.elena.thecorner.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.elena.thecorner.entities.Categoria;
import com.elena.thecorner.entities.Post;
import com.elena.thecorner.entities.Recurso;
import com.elena.thecorner.requests.post.ActualizarPostRequest;
import com.elena.thecorner.requests.post.BorrarPostRequest;
import com.elena.thecorner.requests.post.BuscarPostRequest;
import com.elena.thecorner.requests.post.PostRequest;
import com.elena.thecorner.requests.post.SeleccionarPostRequest;
import com.elena.thecorner.requests.post.SeleccionarPostsPorIdCategoriaRequest;
import com.elena.thecorner.responses.MasterResponse;
import com.elena.thecorner.responses.post.BuscarPostResponse;
import com.elena.thecorner.responses.post.PostsOrdenadoPorFechaResponse;
import com.elena.thecorner.responses.post.SeleccionarPostResponse;
import com.elena.thecorner.responses.post.SeleccionarPostsPorIdCategoriaResponse;
import com.elena.thecorner.services.interfaces.ICategoriaService;
import com.elena.thecorner.services.interfaces.IPostService;
import com.elena.thecorner.services.interfaces.IRecursoService;
import com.google.gson.Gson;

@RestController
@RequestMapping(path = "post")
public class PostController {
	
	@Autowired
	IPostService postService;
	
	@Autowired
	IRecursoService recursoService;
	
	@Autowired
	ICategoriaService categoriaService;
	
	@RequestMapping(path = "crearPost", consumes = "application/json")
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
	public Object crearPost(@RequestBody String json) {
		
		PostRequest request = new Gson().fromJson(json, PostRequest.class);
		MasterResponse response = new MasterResponse();
		
		try {
			
			String postCompleto = request.getParrafos().stream().map(Object::toString).collect(Collectors.joining(" @& "));
			Categoria categoria = categoriaService.findById(request.getIdCategoria()).get();
			
			Post post = new Post();
			post.setTitulo(request.getTitulo());
			post.setPost(postCompleto);
			post.setCategoria(categoria);
			
			postService.save(post);
			
			for(int i = 0; i < request.getRecursosUrls().size(); i++) {
				Recurso recurso = new Recurso();
				recurso.setRecurso(request.getRecursosUrls().get(i));
				String recursoS = request.getRecursosUrls().get(i);
				String sub = recursoS.substring(recursoS.length()-3,recursoS.length());
				recurso.setTipoRecurso(sub);
				recurso.setPost(post);
				recursoService.save(recurso);
			}
			
			response.setId(post.getIdPost());
			response.setMensaje("El post se creó exitosamente.");
			response.setExitoso(true);
			
		}catch(Exception e) {
			response.setMensaje("Ocurrió un error al tratar de crear el post.");
			response.setExitoso(false);
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(path = "actualizarPost", consumes = "application/json")
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
	public Object actualizarPost(@RequestBody String json) {
		
		ActualizarPostRequest request = new Gson().fromJson(json, ActualizarPostRequest.class);
		MasterResponse response = new MasterResponse();
		
		try {
			Post postEncontrado = postService.findById(request.getIdPost()).get(); 
			
			String postCompleto = request.getParrafos().stream().map(Object::toString).collect(Collectors.joining(" @& "));
			Categoria categoria = categoriaService.findById(request.getIdCategoria()).get();
			
			postEncontrado.setTitulo(request.getTitulo());
			postEncontrado.setPost(postCompleto);
			postEncontrado.setCategoria(categoria);
			
			postService.save(postEncontrado);
			
			recursoService.deleteRecursobyidPost(request.getIdPost());
			
			for(int i = 0; i < request.getRecursosUrls().size(); i++) {
				Recurso recurso = new Recurso();
				recurso.setRecurso(request.getRecursosUrls().get(i));
				String recursoS = request.getRecursosUrls().get(i);
				String sub = recursoS.substring(recursoS.length()-3,recursoS.length());
				recurso.setTipoRecurso(sub);
				recurso.setPost(postEncontrado);
				recursoService.save(recurso);
			}
			
			response.setMensaje("El post ha sido actualizado exitosamente.");
			response.setExitoso(true);
		}catch(Exception e){
			response.setMensaje("Ocurrió un problema al tratar de actualizar el post.");
			response.setExitoso(false);
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(path = "borrarPost", consumes = "application/json")
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
	public Object borrarPost(@RequestBody String json) {
		
		BorrarPostRequest request = new Gson().fromJson(json, BorrarPostRequest.class);
		MasterResponse response = new MasterResponse();
		
		try {
			postService.deleteById(request.getIdPost());
			
			response.setMensaje("El post ha sido borrado de manera exitosa.");
			response.setExitoso(true);
			
		}catch(Exception e) {
			response.setMensaje("Ocurrió un error al tratar de borrar el post.");
			response.setExitoso(false);
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(path = "seleccionarPost", consumes = "application/json")
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
	public Object seleccionarPost(@RequestBody String json) {
		
		SeleccionarPostRequest request = new Gson().fromJson(json, SeleccionarPostRequest.class);
		SeleccionarPostResponse response = new SeleccionarPostResponse();
		
		try {
			Post postEncontrado = postService.findById(request.getIdPost()).get();
			
			String parrafos[] = postEncontrado.getPost().split(" @& ");			
			List<String> parrafosList = new ArrayList<>();
			
			for(int i = 0; i < parrafos.length; i++) {
				parrafosList.add(parrafos[i]);
			}
									
			List<Recurso> recursos = recursoService.seleccionarRecurso(request.getIdPost());
			List<String> recursosUrls = new ArrayList<>();
			List<String> recursosTipos = new ArrayList<>();
			
			for(int i = 0; i < recursos.size(); i++) {
				recursosUrls.add(recursos.get(i).getRecurso());
				recursosTipos.add(recursos.get(i).getTipoRecurso());
			}
			
			response.setRecursosUrls(recursosUrls);
			response.setParrafos(parrafosList);
			response.setTipoRecurso(recursosTipos);
			response.setTitulo(postEncontrado.getTitulo());
			response.setMensaje("Se pudo seleccionar el post.");
			response.setExitoso(true);		
			
		}catch(Exception e) {
			response.setMensaje("Ocurrió un error al seleccionar el post.");
			response.setExitoso(false);
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(path = "postsOrdenadoPorFecha", consumes = "application/json")
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
	public Object postsOrdenadoPorFecha(@RequestBody String json) {
			
		List<PostsOrdenadoPorFechaResponse> responseList = new ArrayList<>();
		List<Post> postsOrdenados = postService.devolverPostsOrdenadosPorFecha();
		
		for(int i = 0; i < postsOrdenados.size(); i++) {
			PostsOrdenadoPorFechaResponse response = new PostsOrdenadoPorFechaResponse();
			try {
				
				String parrafos[] = postsOrdenados.get(i).getPost().split(" @& ");
				List<String> parrafosList = new ArrayList<>();
				
				parrafosList.add(parrafos[0]);//Ésto es el primer párrafo
				
				List<Recurso> recursos = recursoService.seleccionarRecurso(postsOrdenados.get(i).getIdPost());
				List<String> recursosUrls = new ArrayList<>();
				recursosUrls.add(recursos.get(0).getRecurso());
				
				String nombreCategoria = postsOrdenados.get(i).getCategoria().getNombreCategoria();
				String fechaPost = postsOrdenados.get(i).getCreatedAt().toString();
				String titulo = postsOrdenados.get(i).getTitulo();
				long idPost = postsOrdenados.get(i).getIdPost();
				long idCategoria = postsOrdenados.get(i).getCategoria().getIdCategoria();
				
				response.setNombreCategoria(nombreCategoria);
				response.setCreatedAt(fechaPost);
				response.setParrafos(parrafosList);
				response.setRecursosUrls(recursosUrls);
				response.setTitulo(titulo);
				response.setIdPost(idPost);
				response.setId(idPost);
				response.setIdCategoria(idCategoria);
				response.setMensaje("Se ordenan los posts de manera correcta.");
				response.setExitoso(true);
				
				responseList.add(response);
			}catch(Exception e) {
				response.setMensaje("Algo salió mal.");
				response.setExitoso(false);
				e.printStackTrace();
			}
			
		}
		return responseList;
	}
	
	@RequestMapping(path = "seleccionarPostsPorIdCategoria", consumes = "application/json")
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
	public Object seleccionarPostsPorIdCategoria(@RequestBody String json) {
		
		SeleccionarPostsPorIdCategoriaRequest request = new Gson().fromJson(json, SeleccionarPostsPorIdCategoriaRequest.class);
		
		List<SeleccionarPostsPorIdCategoriaResponse> responseList = new ArrayList<>();
		List<Post> postsSeleccionados = postService.seleccionarPostsByIdCategoria(request.getIdCategoria());
		
		for(int i = 0; i < postsSeleccionados.size(); i++) {
			SeleccionarPostsPorIdCategoriaResponse response = new SeleccionarPostsPorIdCategoriaResponse();
			try {
				String parrafos[] = postsSeleccionados.get(i).getPost().split(" @& ");
				List<String> parrafosList = new ArrayList<>();
				
				parrafosList.add(parrafos[0]);//Ésto es el primer párrafo
				
				List<Recurso> recursos = recursoService.seleccionarRecurso(postsSeleccionados.get(i).getIdPost());
				List<String> recursosUrls = new ArrayList<>();
				recursosUrls.add(recursos.get(0).getRecurso());
				
				String nombreCategoria = postsSeleccionados.get(i).getCategoria().getNombreCategoria();
				String fechaPost = postsSeleccionados.get(i).getCreatedAt().toString();
				String titulo = postsSeleccionados.get(i).getTitulo();
				long idPost = postsSeleccionados.get(i).getIdPost();
				
				response.setNombreCategoria(nombreCategoria);
				response.setCreatedAt(fechaPost);
				response.setParrafos(parrafosList);
				response.setRecursosUrls(recursosUrls);
				response.setTitulo(titulo);
				response.setIdPost(idPost);				
				
				response.setMensaje("Se muestra correctamente todos los posts de la categoría seleccionada.");
				response.setExitoso(true);
				
				responseList.add(response);					
			}catch(Exception e){
				response.setMensaje("Algo salió mal");
				response.setExitoso(false);
				e.printStackTrace();
			}
			
		}			
		return responseList;
	}
	
	@RequestMapping(path = "buscarPost", consumes = "application/json")
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
	public Object buscarPost(@RequestBody String json) {
		
		BuscarPostRequest request = new Gson().fromJson(json, BuscarPostRequest.class);
			
			List<BuscarPostResponse> responseList = new ArrayList<>();
			List<Post> buscarPost = postService.devolverTitulosDePosts(request.getTitulo());
			
			
			for(int i = 0; i < buscarPost.size(); i++ ) {
				BuscarPostResponse response = new Gson().fromJson(json, BuscarPostResponse.class);
				try {
					String titulo = buscarPost.get(i).getTitulo();
					long idPost = buscarPost.get(i).getIdPost();
					
					List<Recurso> recursosEncontrado = recursoService.seleccionarRecurso(buscarPost.get(i).getIdPost());
					List<String> recursosUrls = new ArrayList<>();
					recursosUrls.add(recursosEncontrado.get(0).getRecurso());
					
					response.setTitulo(titulo);
					response.setRecursosUrls(recursosUrls);
					response.setMensaje("Todo salió bien.");
					response.setIdPost(idPost);
					response.setExitoso(true);
					
					responseList.add(response);	
					
				}catch(Exception e) {
					response.setMensaje("Algo salió mal.");
					response.setExitoso(false);
					e.printStackTrace();
				}
			}
		return responseList;
	}
		
}
