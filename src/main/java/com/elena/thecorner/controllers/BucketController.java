package com.elena.thecorner.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.elena.thecorner.components.AmazonComponent;
import com.elena.thecorner.requests.post.DeleteFileRequest;
import com.google.gson.Gson;



@RestController
public class BucketController {
	@Autowired
	private AmazonComponent amazonComponent;
	
	@PostMapping("/uploadFile")
	@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST}, allowedHeaders = {"Origin", "Content-Type", "X-Auth-Token"})
	public String uploadFile(@RequestPart(value = "file") MultipartFile file) {
		String finalPath = this.amazonComponent.uploadFile(file); 
		return "{\"url\":\""+finalPath+"\"}";
	}
	
	@PostMapping("/deleteFile")
	@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST}, allowedHeaders = {"Origin", "Content-Type", "X-Auth-Token"})
	public String deleteFile(@RequestBody String json) {
		DeleteFileRequest request = new Gson().fromJson(json, DeleteFileRequest.class);
		this.amazonComponent.deleteFileFromS3Bucket(request.getFileName());
        return "{\"fileDeleted\":\""+request.getFileName()+"\"}";
    }
}
