package com.elena.thecorner.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.elena.thecorner.entities.Comentario;
import com.elena.thecorner.requests.comentarios.ComentarioRequest;
import com.elena.thecorner.responses.MasterResponse;
import com.elena.thecorner.services.interfaces.IComentarioService;
import com.google.gson.Gson;

@RestController
@RequestMapping(path = "comentario")
public class ComentarioController {
	
	@Autowired
	IComentarioService comentarioService;
	
	@RequestMapping(path = "registroComentario", consumes = "application/json")
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
	public Object registroComentario(@RequestBody String json) {
		
		ComentarioRequest request = new Gson().fromJson(json, ComentarioRequest.class);
		MasterResponse response = new MasterResponse();
		
		try {
			Comentario comentario = new Comentario();
			comentario.setNombre(request.getNombre());
			comentario.setComentario(request.getComentario());
			
			comentarioService.save(comentario);
			
			response.setMensaje("El comentario se creó exitosamente.");
			response.setExitoso(true);
			
		}catch(Exception e) {
			response.setMensaje("Ocurrió un error al tratar de crear el comentario");
			response.setExitoso(false);
			e.printStackTrace();
		}
		return response;
	}

}
