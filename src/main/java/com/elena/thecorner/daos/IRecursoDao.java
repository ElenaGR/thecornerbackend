package com.elena.thecorner.daos;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.elena.thecorner.entities.Recurso;

@Repository
public interface IRecursoDao extends CrudRepository<Recurso, Long>{
	
	@Modifying
	@Transactional
	@Query(value = "DELETE FROM recursos WHERE recursos.id_post = ?1", nativeQuery = true)
	public void deleteRecursobyidPost(Long idPost);
	
	@Query(value = "SELECT * FROM recursos WHERE recursos.id_post = ?1", nativeQuery = true)
	public List<Recurso> seleccionarRecurso(Long idPost);
}
