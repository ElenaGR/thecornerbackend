package com.elena.thecorner.daos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.elena.thecorner.entities.Categoria;

@Repository
public interface ICategoriaDao extends CrudRepository<Categoria, Long>{

}
