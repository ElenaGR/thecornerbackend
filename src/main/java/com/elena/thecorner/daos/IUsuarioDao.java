package com.elena.thecorner.daos;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.elena.thecorner.entities.Usuario;

@Repository
public interface IUsuarioDao extends CrudRepository<Usuario, Long>{

	@Query(value = "SELECT * FROM usuarios WHERE usuarios.correo = ?1", nativeQuery = true)
	public Usuario encontarUsuarioPorCorreo(String correo);
}
