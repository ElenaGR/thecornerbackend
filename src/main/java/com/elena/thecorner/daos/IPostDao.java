package com.elena.thecorner.daos;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.elena.thecorner.entities.Post;

@Repository
public interface IPostDao extends CrudRepository<Post, Long>{
	
	@Query(value = "SELECT * FROM posts WHERE posts.id_categoria = ?1 ORDER BY created_at DESC LIMIT 12", nativeQuery = true)
	public List<Post> seleccionarPostsByIdCategoria(Long idCategoria);
	
	@Query(value = "SELECT * FROM posts ORDER BY created_at DESC LIMIT 12", nativeQuery = true)
	public List<Post> devolverPostsOrdenadosPorFecha();
	
	@Query(value = "SELECT * FROM posts WHERE posts.titulo LIKE %?1%", nativeQuery = true)
	public List<Post> devolverTitulosDePosts(String titulo);
}
