package com.elena.thecorner.daos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.elena.thecorner.entities.Comentario;

@Repository
public interface IComentarioDao extends CrudRepository<Comentario, Long>{
	
}
