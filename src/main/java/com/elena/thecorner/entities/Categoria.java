package com.elena.thecorner.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "categorias")
public class Categoria extends AuditModel{

	private static final long serialVersionUID = -7684118959159069380L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long idCategoria;
	
	String nombreCategoria;

	public Long getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Long idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}
	
	
	/*Relations*/
	
	@OneToMany(mappedBy = "categoria", fetch = FetchType.LAZY)
	@JsonManagedReference
	List<Post> categoriaPost;

	public List<Post> getCategoriaPost() {
		return categoriaPost;
	}

	public void setCategoriaPost(List<Post> categoriaPost) {
		this.categoriaPost = categoriaPost;
	}
}
