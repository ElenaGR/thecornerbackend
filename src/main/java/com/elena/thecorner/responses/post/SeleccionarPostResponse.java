package com.elena.thecorner.responses.post;

import java.util.List;

import com.elena.thecorner.responses.MasterResponse;

public class SeleccionarPostResponse extends MasterResponse{
	private List<String> parrafos;
	
	private List<String> recursosUrls;
	
	private List<String> tipoRecurso;

	private String titulo;

	public List<String> getTipoRecurso() {
		return tipoRecurso;
	}

	public void setTipoRecurso(List<String> tipoRecurso) {
		this.tipoRecurso = tipoRecurso;
	}

	public List<String> getParrafos() {
		return parrafos;
	}

	public void setParrafos(List<String> parrafos) {
		this.parrafos = parrafos;
	}

	public List<String> getRecursosUrls() {
		return recursosUrls;
	}

	public void setRecursosUrls(List<String> recursosUrls) {
		this.recursosUrls = recursosUrls;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
}
