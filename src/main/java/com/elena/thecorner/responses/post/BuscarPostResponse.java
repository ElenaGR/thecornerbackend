package com.elena.thecorner.responses.post;

import java.util.List;

import com.elena.thecorner.responses.MasterResponse;

public class BuscarPostResponse extends MasterResponse{
	private String titulo;
	
	private List<String> recursosUrls;
	
	long idPost;

	public long getIdPost() {
		return idPost;
	}

	public void setIdPost(long idPost) {
		this.idPost = idPost;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<String> getRecursosUrls() {
		return recursosUrls;
	}

	public void setRecursosUrls(List<String> recursosUrls) {
		this.recursosUrls = recursosUrls;
	}
}
