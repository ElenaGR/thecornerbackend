package com.elena.thecorner.responses.post;

import java.util.List;

import com.elena.thecorner.responses.MasterResponse;

public class PostsOrdenadoPorFechaResponse extends MasterResponse{
	private List<String> parrafos;
	
	private List<String> recursosUrls;
	
	private List<String> tipoRecurso;
	
	private String createdAt;
	
	private String nombreCategoria;
	
	private String titulo;
	
	private long idPost;
	
	private long idCategoria;

	public long getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(long idCategoria) {
		this.idCategoria = idCategoria;
	}

	public long getIdPost() {
		return idPost;
	}

	public void setIdPost(long idPost) {
		this.idPost = idPost;
	}

	public List<String> getParrafos() {
		return parrafos;
	}

	public void setParrafos(List<String> parrafos) {
		this.parrafos = parrafos;
	}

	public List<String> getRecursosUrls() {
		return recursosUrls;
	}

	public void setRecursosUrls(List<String> recursosUrls) {
		this.recursosUrls = recursosUrls;
	}

	public List<String> getTipoRecurso() {
		return tipoRecurso;
	}

	public void setTipoRecurso(List<String> tipoRecurso) {
		this.tipoRecurso = tipoRecurso;
	}
	
	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
}
